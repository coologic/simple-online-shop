CREATE TABLE IF NOT EXISTS commodity (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    price INT not null,
    unit varchar(32) not null,
    picture_url varchar(256) not null
);