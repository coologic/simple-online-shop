package org.onlineshop.webApp.service;

import org.onlineshop.webApp.contract.*;
import org.onlineshop.webApp.domain.Commodity;
import org.onlineshop.webApp.domain.CommodityRepository;
import org.onlineshop.webApp.domain.ShopOrder;
import org.onlineshop.webApp.domain.ShopOrderRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShopService {
    private final CommodityRepository commodityRepository;
    private final ShopOrderRepository shopOrderRepository;

    public ShopService(CommodityRepository commodityRepository,
                       ShopOrderRepository shopOrderRepository) {
        this.commodityRepository = commodityRepository;
        this.shopOrderRepository = shopOrderRepository;
    }

    public void create(CreateCommodityRequest request) {
        commodityRepository.saveAndFlush(
                new Commodity(request.getName(),
                        request.getPrice(),
                        request.getUnit(),
                        request.getPictureUrl()));
    }

    public List<CommodityResponse> getAll() {
        List<Commodity> commodityList = commodityRepository.findAll();
        List<CommodityResponse> response = new ArrayList<>();
        for (Commodity commodity :
                commodityList) {
            response.add(new CommodityResponse(commodity.getId(),
                    commodity.getName(),
                    commodity.getPrice(),
                    commodity.getUnit(),
                    commodity.getPictureUrl()));
        }
        return response;
    }

    public void addOrder(BuyCommodityRequest request) {
        Commodity commodity = commodityRepository
                .findById(request.getId())
                .orElseThrow(IllegalArgumentException::new);
        if (request.getName() == null || !request.getName().equals(commodity.getName())) {
            throw new IllegalArgumentException("物品id与名称不匹配");
        }
        shopOrderRepository.saveAndFlush(new ShopOrder(commodity.getId(),
                commodity.getName(),
                commodity.getPrice(),
                commodity.getUnit(),
                request.getNumber()));
    }

    public List<OrderResponse> getOrders() {
        List<ShopOrder> orders = shopOrderRepository.findAll();
        List<OrderResponse> response = new ArrayList<>();
        for (ShopOrder order : orders) {
            response.add(new OrderResponse(order.getId(),
                    order.getCommodityName(),
                    order.getCommodityPrice(),
                    order.getBuyNumber(),
                    order.getCommodityUnit()));
        }
        return response;
    }

    public void deleteOrder(Long id) {
        shopOrderRepository.deleteById(id);
        shopOrderRepository.flush();
    }
}
