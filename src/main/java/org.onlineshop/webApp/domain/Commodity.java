package org.onlineshop.webApp.domain;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
public class Commodity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false)
    private Long id;

    @Column(nullable = false, length = 128)
    private String name;
    @Column(nullable = false)
    private Integer price;
    @Column(nullable = false, length = 256)
    private String unit;
    @Column
    private String pictureUrl;

    public Commodity() {
    }

    public Commodity(String name, Integer price, String unit, String pictureUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureUrl = pictureUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }
}

