package org.onlineshop.webApp.contract;

@SuppressWarnings("unused")
public class OrderResponse {
    private Long id;
    private String name;
    private Integer price;
    private Integer number;
    private String unit;

    public OrderResponse() {
    }

    public OrderResponse(Long id,
                         String name,
                         Integer price,
                         Integer number,
                         String unit) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.number = number;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getNumber() {
        return number;
    }

    public String getUnit() {
        return unit;
    }
}
