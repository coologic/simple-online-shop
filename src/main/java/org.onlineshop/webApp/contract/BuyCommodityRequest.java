package org.onlineshop.webApp.contract;

import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
public class BuyCommodityRequest {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private Integer number;

    public BuyCommodityRequest() {
    }

    public BuyCommodityRequest(Long id, String name, Integer number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getNumber() {
        return number;
    }
}
