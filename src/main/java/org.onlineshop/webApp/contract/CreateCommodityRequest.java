package org.onlineshop.webApp.contract;

import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
public class CreateCommodityRequest {
    @NotNull
    private String name;
    @NotNull
    private Integer price;
    @NotNull
    private String unit;
    private String pictureUrl;

    public CreateCommodityRequest() {
    }

    public CreateCommodityRequest(String name, Integer price, String unit, String pictureUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureUrl = pictureUrl;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }
}
