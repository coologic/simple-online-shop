package org.onlineshop.webApp.contract;

@SuppressWarnings("unused")
public class CommodityResponse {
    private Long id;
    private String name;
    private Integer price;
    private String unit;
    private String pictureUrl;

    public CommodityResponse() {
    }

    public CommodityResponse(Long id, String name, Integer price, String unit, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureUrl = pictureUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }
}
