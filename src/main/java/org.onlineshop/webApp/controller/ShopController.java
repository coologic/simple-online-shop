package org.onlineshop.webApp.controller;

import org.onlineshop.webApp.contract.*;
import org.onlineshop.webApp.service.ShopService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ShopController {
    private final ShopService service;

    public ShopController(ShopService shopService) {
        this.service = shopService;
    }


    @PostMapping("/commodities")
    @ResponseStatus(HttpStatus.CREATED)
    void create(@RequestBody @Valid CreateCommodityRequest request) {
        service.create(request);
    }

    @GetMapping("/commodities")
    ResponseEntity<List<CommodityResponse>> getAll() {
        List<CommodityResponse> response = service.getAll();
        return ResponseEntity
                .status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(response);
    }

    @PostMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    void addOrder(@RequestBody @Valid BuyCommodityRequest request) {
        service.addOrder(request);
    }

    @ExceptionHandler({IllegalArgumentException.class})
    ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException exception) {
        return ResponseEntity.status(400)
                .body(exception.getMessage());
    }

    @GetMapping("/orders")
    ResponseEntity<List<OrderResponse>> getOrders() {
        return  ResponseEntity
                .status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(service.getOrders());
    }

    @DeleteMapping("/orders/{id}")
    ResponseEntity<Object> deleteOrder(@PathVariable Long id) {
        service.deleteOrder(id);
        return  ResponseEntity.status(200).build();
    }
}
