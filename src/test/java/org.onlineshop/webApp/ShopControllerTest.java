package org.onlineshop.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.onlineshop.webApp.contract.BuyCommodityRequest;
import org.onlineshop.webApp.contract.CreateCommodityRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ShopControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_201_when_create_commodity_with_name_and_price_and_unit()
            throws Exception {
        String whateverName = "name";
        int whateverPrice = 123;
        String whateverUnit = "个";
        String whateverUrl = "";
        createCommodity(whateverName, whateverPrice, whateverUnit, whateverUrl)
                .andExpect(status().is(201));
    }

    private ResultActions createCommodity(String name, int price, String unit, String url) throws Exception {
        return mockMvc.perform(post("/api/commodities")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        new CreateCommodityRequest(name,
                                price,
                                unit,
                                url))));
    }

    @Test
    void should_get_commodities_when_get_from_two_shop_store() throws Exception {
        String whateverName1 = "name1";
        String whateverName2 = "name2";
        int whateverPrice = 123;
        String whateverUnit = "个";
        String whateverUrl = "";
        createCommodity(whateverName1, whateverPrice, whateverUnit, whateverUrl);
        createCommodity(whateverName2, whateverPrice, whateverUnit, whateverUrl);

        mockMvc.perform(get("/api/commodities"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name", Matchers.is(whateverName1)))
                .andExpect(jsonPath("$[1].name", Matchers.is(whateverName2)));
    }

    @Test
    void should_get_200_when_add_order() throws Exception {
        String whateverName1 = "name1";
        String whateverName2 = "name2";
        int whateverPrice = 123;
        String whateverUnit = "个";
        String whateverUrl = "";
        createCommodity(whateverName1, whateverPrice, whateverUnit, whateverUrl);
        createCommodity(whateverName2, whateverPrice, whateverUnit, whateverUrl);

        addOrder(whateverName1, 1L, 2)
                .andExpect(status().is(200));
    }

    @Test
    void should_get_400_when_add_undefined_commodity_to_orders() throws Exception {
        addOrder("name1", 1L, 2)
                .andExpect(status().is(400));
    }

    @Test
    void should_get_400_when_add_with_id_and_name_dismatch_commodity() throws Exception {
        String whateverName = "name1";
        String whateverErrorName = "name2";
        int whateverPrice = 123;
        String whateverUnit = "个";
        String whateverUrl = "";
        createCommodity(whateverName, whateverPrice, whateverUnit, whateverUrl);

        addOrder(whateverErrorName, 1L, 2)
                .andExpect(status().is(400));
    }

    private ResultActions addOrder(String name, long id, int buyNumber) throws Exception {
        return mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        new BuyCommodityRequest(id, name, buyNumber))));
    }

    @Test
    void should_get_orders() throws Exception {
        String whateverFirstName = "name1";
        createCommodity(whateverFirstName, 123, "个", "");
        String whateverSecondName = "name2";
        createCommodity(whateverSecondName, 123, "个", "");
        addOrder("name1", 1L, 2);
        addOrder("name2", 2L, 3);

        mockMvc.perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name", Matchers.is(whateverFirstName)))
                .andExpect(jsonPath("$[1].name", Matchers.is(whateverSecondName)));
    }

    @Test
    void should_get_only_first_order_when_delete_first_of_two_orders() throws Exception {
        String whateverFirstName = "name1";
        createCommodity(whateverFirstName, 123, "个", "");
        String whateverSecondName = "name2";
        createCommodity(whateverSecondName, 123, "个", "");
        addOrder("name1", 1L, 2);
        addOrder("name2", 2L, 3);

        mockMvc.perform(delete("/api/orders/2"))
                .andExpect(status().is(200));

        mockMvc.perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(content().string("[{\"id\":1," +
                        "\"name\":\"name1\"," +
                        "\"price\":123," +
                        "\"number\":2," +
                        "\"unit\":\"个\"}]"));

    }
}
